# Docker ros-nvidia-moveit-full:indigo
# Overview

This repository is used to build a docker repository, which is located in the
docker cloud under feesmrt/ros-nvidia-moveit-full:indigo.
</br>  
To be able to run tools like RVIZ with hardware accelleration the nvidia-docker
modification is used.
</br>
This repository contains a full MoveIt! installation and a few components from
ROSControl. You can use it to develop MoveIt! Applications.

# Installation

1. you've to install [Docker](https://www.docker.com/) on you're machine.
   A Tutorial can be found
   [here](https://docs.docker.com/engine/installation/linux/ubuntulinux/).

2. you've to install nvidia-docker to enable hardware accelleration.
   The installation guide can be found on the
   [GitHub](https://github.com/NVIDIA/nvidia-docker) page of the project.

3. you've to build the image with the provided dockerfile in this repository
   or pull it from the docker cloud.

## Alternative: Install script

You can use the install script in this repository if you have ubuntu installed.
Please be careful. The script was only tested at Ubuntu 16.04.
> Install script
```
sudo ./install.sh
```

## Building or Pulling image

> Building: Run the following command in the root directory of the repository:
```
docker build -t ros-nvidia-moveit-full:indigo .
```
</br>  

> Pulling from docker hub: Run the following command
```
docker pull feesmrt/ros-nvidia-moveit-full:indigo
```

If you want to use the shell scripts you've to set the right to execute these.
```
sudo chmod +x docker-control.sh
```

# Running

## Starting your image

There is a shell script to start the docker image.

> Starting image
```
./docker-control.sh -s
```

There are two arguments you can use for the start script:
- The first argument -nv enables hardware accelleration support, because it uses
nvidia-docker.
- The second argument -ws enables you to link a local folder of the host machine
to the workspace folder of the image.
The folder is linked with the /home/rosuser/catkin_ws folder of the image.
- The third argument -vo enables you to link any folder you want to an image folder.

> Starting image with nvidia hardware accelleration
```
./docker-control.sh -s -nv
```
> Starting image with hardware accelleration and link to your local workspace
```
./docker-control.sh -s -nv -ws /home/user/local_catkin_ws
```
> Starting image with link to a individual folder
```
./docker-control.sh -s -vo /home/host/documents:/home/rosuser/documents
```

## Connecting a new bash windows to image

If you want to open a new bash, because for example you running roscore in the
first bash instance, you can use this command in a new terminal window.

> Conneting new bash window to the running image: Open a new windows with (STRG + LSHIFT + T)
and run:
```
./docker-control.sh -a
```

## Cleanup

If you want to delete all runned repositories you can run another script:

> Cleanup exited images

```
./docker-control.sh -c
```

# Note

## User data

The image has an user with sudo rights.

> User data
```
username: rosuser
password: rosuser
```

## Running default docker image

If you want to use your own docker image you've to modify the $DOCKER_IMAGE
variable in the script docker-control.sh.

> Running default image with the shell script (e.g. ubuntu 14.04) use the parameter
-i.
```
./docker-control -s -im ubuntu:14.04
```

> Running local docker image with shell script edit docker-control.sh
```
10  # Variables for names
11  VERSION=1.0
12  AUTHOR="Martin Fees"
13  EMAIL="martin.fees@gmx.de"
14  
15  # Docker image
16  DOCKER_IMAGE=feesmrt/ros-nvidia-base:indigo # Replace this with you image name!
```
